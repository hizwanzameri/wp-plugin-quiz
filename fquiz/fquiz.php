<?php
    /*
     * Plugin Name: fQuiz
     * Description: Custom quiz WP plugin
     * Version: 1.0
     * Author: Fulkrum Interactive
     * Author URI: http://fulkrum.net
     */
    
    function fquiz_wp_shortcode($atts, $content=null)
    {
        ob_start();
        include 'quiz.php';
        return ob_get_clean();
    }
    add_shortcode('fquiz', 'fquiz_wp_shortcode');
    ?>
