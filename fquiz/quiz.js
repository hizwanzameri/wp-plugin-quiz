var q1score = '';
var q2score = '';
var q3score = '';
var q4score = '';
var q5score = '';
var q6score = '';
var q7score = '';
var q8score = '';

var q1id = '';
var q2id = '';
var q3id = '';
var q4id = '';
var q5id = '';
var q6id = '';
var q7id = '';
var q8id = '';



$('#insuredoption').change(function(){
var sVal = $(this).val();                                                          
if(sVal=='same'){
        $('#IName').hide();
        $('#IName').text('Same');
    }else{
        $('#IName').text('');
        $('#IName').show();
    }
});
/*
$('#insuredoption')
  .change(function () {
    var str = "";
    $( "select option:selected" ).each(function() {
      str += $( this ).text() + " ";
    });
    $( "INametest" ).text( str );
    if(str=='Same'){
        $('#IName').hide();
        $('#IName').text('Same');
    }else{
        $('#IName').text('');
        $('#IName').show();
    }
  })
  .change();
  */

$('#q1answers input').on('change', function() {
   var $qradio = $('input[name=question1]:checked', '#q1answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q1score = qval;
    q1id = qid;
});

$('#q2answers input').on('change', function() {
   var $qradio = $('input[name=question2]:checked', '#q2answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q2score = qval;
    q2id = qid;
});

$('#q3answers input').on('change', function() {
   var $qradio = $('input[name=question3]:checked', '#q3answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q3score = qval;
    q3id = qid;
});

$('#q4answers input').on('change', function() {
   var $qradio = $('input[name=question4]:checked', '#q4answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q4score = qval;
    q4id = qid;
});

$('#q5answers input').on('change', function() {
   var $qradio = $('input[name=question5]:checked', '#q5answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q5score = qval;
    q5id = qid;
});

$('#q6answers input').on('change', function() {
   var $qradio = $('input[name=question6]:checked', '#q6answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q6score = qval;
    q6id = qid;
});

$('#q7answers input').on('change', function() {
   var $qradio = $('input[name=question7]:checked', '#q7answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q7score = qval;
    q7id = qid;
});


$('#q8answers input').on('change', function() {
   var $qradio = $('input[name=question8]:checked', '#q8answers'); 
    var qval = $qradio.val();
    var qid = $qradio.attr('id');
    q8score = qval;
    q8id = qid;
});

function showResult() {

    $('#qMessage').text('<tr><td style="border: 1px solid black;border-collapse: collapse;">1</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q1id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q1score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">2</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q2id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q2score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">3</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q3id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q3score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">4</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q4id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q4score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">5</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q5id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q5score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">6</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q6id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q6score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">7</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q7id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q7score + '</td></tr><tr><td style="border: 1px solid black;border-collapse: collapse;">6</td><td style="border: 1px solid black;border-collapse: collapse;">' + $('#'+ q8id + 'label').html() + '</td><td style="border: 1px solid black;border-collapse: collapse;">' + q8score + '</td></tr>');

    var part1sum = parseInt(q2score) + parseInt(q3score);
    var part2sum = parseInt(q4score) + parseInt(q5score) + parseInt(q6score) + parseInt(q7score) + parseInt(q7score);
    var totalscore = parseInt(part1sum) + parseInt(part2sum);
    $('#part1totalall').text(part1sum);
    $('#part2total').text(part2sum);
    $('#totalscore').text(totalscore);
}




$('#q1btn').on("click", function () {
    if ($('input[name=question1]:checked').length) {
        $('#Q1').hide();
        $('#Q2').show();
        if(q1score=='1'){
            $('#q2_1label').text('64 or less ');
            $('#q2_2label').text('65-74 years old');
            $('#q2_3label').text('75-78 years old');
            $('#q2_4label').text('79-83 years old');
            $('#q2_0label').text('84 years or older ');
        }else{
            $('#q2_1label').text('64 or less ');
            $('#q2_2label').text('65-77 years old ');
            $('#q2_3label').text('78-81 years old ');
            $('#q2_4label').text('82-86 years old ');
            $('#q2_0label').text('87 years or older ');
        }
    } else {
        $('#warnq1').show();
    }
});

$('#q2btn').on("click", function () {
    if ($('input[name=question2]:checked').length) {
       $('#Q2').hide();
       $('#Q3').show();
       
    } else {
        $('#warnq2').show();
    }

});

$('#q3btn').on("click", function () {
    if ($('input[name=question3]:checked').length) {
        var part1sum = parseInt(q2score) + parseInt(q3score);
       if(part1sum >= 4){
        $('#Q3').hide();
        $('#Q4').show();
        }
        else{
            $('#Q3').hide();
            $('#resultpart1').show();
        }
        
    } else {
        $('#warnq3').show();
    }

});

$('#q4btn').on("click", function () {
    if ($('input[name=question4]:checked').length) {
        $('#Q4').hide();
        $('#Q5').show();
        
    } else {
        $('#warnq4').show();
    }

});

$('#q5btn').on("click", function () {
    if ($('input[name=question5]:checked').length) {
        $('#Q5').hide();
        $('#Q6').show();
        
    } else {
        $('#warnq5').show();
    }


});

$('#q6btn').on("click", function () {
    if ($('input[name=question6]:checked').length) {

        showResult();
        $('#Q6').hide();
        $('#result').show();

        
    } else {
        $('#warnq6').show();
    }

});


//back button
$('#q2btnback').on("click", function () {

        $('#Q2').hide();
        $('#Q1').show();


});

$('#q3btnback').on("click", function () {

        $('#Q3').hide();
        $('#Q2').show();

});

$('#q4btnback').on("click", function () {

        $('#Q4').hide();
        $('#Q3').show();

});

$('#q5btnback').on("click", function () {

        $('#Q5').hide();
        $('#Q4').show();

});

$('#q6btnback').on("click", function () {

        $('#Q6').hide();
        $('#Q5').show();

});

$('#q7btnback').on("click", function () {

        $('#Q7').hide();
        $('#Q6').show();

});

$('#q8btnback').on("click", function () {

        $('#Q8').hide();
        $('#Q7').show();

});

$("#submitall").click(function(){

var allScore = parseInt(q4score) + parseInt(q5score) + parseInt(q6score) + parseInt(q7score) + parseInt(q7score);

var name = $("#qName").val();
var iName = $("#IName").val();
var mailAdd = $("textarea#qAdd").val();
var email = $("#qEmail").val();
var tel = $("#qTel").val();
var body = $("#qMessage").val();

if(allScore >= 15){
var allResult = "<h5>Your Score is "+allScore+"</h5><hr><h1>You are an excellent candidate</h1><span style='border: 1px solid #000000; padding: 20px;'>Congratulations, you are an excellent candidate for a life or viatical settlement! Please download and complete our application and contact us to get started on funding your new future today! (CTA link and listing)</span>";
}

if(allScore < 15){
var allResult = "<h5>Your Score is "+allScore+"</h5><hr><h1>You are a good candidate</h1><span style='border: 1px solid #000000; padding: 20px;'>Congratulations, you are a good candidate for a life or viatical settlement! Please contact us today to learn more about how we can help fund your new future! (CTA link and listing)</span>";
}

if(allScore <= 10){
var allResult = "<h5>Your Score is "+allScore+"</h5><hr><h1>You are not a good candidate</h1><span style='border: 1px solid #000000; padding: 20px;'>While unfortunately at this time, you do not appear to be a good candidate for a life or viatical settlement, there may be other options available. To review your situation in more detail and explore what other options may be available to help fund your new future, please contact us today. (CTA link and listing)</span>";
}

var message = '<div><h4>Name: '+ name +'</h4><h4>Email: '+ email +'</h4><h4>Phone: '+ tel +'</h4><h4>Mailing Address: '+ mailAdd +'</h4><h4>Insured Name: '+ iName +'</h4><hr><div id="result"><h4>Result</h4><table style="border: 1px solid black;border-collapse: collapse;" cellpadding="10"><thead><tr><th style="border: 1px solid black;border-collapse: collapse;">#</th><th style="border: 1px solid black;border-collapse: collapse;">Question</th><th style="border: 1px solid black;border-collapse: collapse;">Score</th></tr></thead><tbody id="resultlist">'+body+'</tbody></table><h4>Total Score: <span>'+ $('#totalscore').html() +'</span></h4><h4>Part 1 Total: <span>'+ $('#part1totalall').html() +'</span></h4><h4>Part 2 Total: <span>'+ $('#part2total').html() +'</span></h4></div>';

// Returns successful data submission message when the entered information is stored in database.
var dataString = 'Imail='+ email +'&IResult='+ escape(allResult) +'&Message='+ escape(message);
if(name==''||email==''||tel=='')
{
alert("Please Fill All Fields");
}
else
{
// AJAX Code To Submit Form.
$.ajax({
type: "POST",
url: "fquizresult.php",
data: dataString,
success: function(){
$('#result').hide();
$('#resultsent').show();
}
});
}
return false;
});
