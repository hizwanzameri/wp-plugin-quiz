<style id='main-style-inline-css' type='text/css'>
.hideqs {
    display:none;
}
.qwarn {
    color: red;
    font-weight: 400;
}
#quizWrap {
    font-family:'Open Sans', sans-serif;
    font-weight: 300;
    background-color: #F1F1F1;
    padding-top: 20px;
    padding-left: 20px;
    padding-right: 20px;
    padding-bottom: 70px;
}
.quizblock {
    position: relative;
}
ul.answers li {
    list-style: none;
}
.btnquiz {
    text-decoration: none;
    color: #ffffff;
    background-color: #1F8DD6;
    padding: 10px;
    border: solid 1px #1F8DD6;
    position: absolute;
    right: 10px;
}
.btnquizprev {
    text-decoration: none;
    color: #ffffff;
    background-color: #1F8DD6;
    padding: 10px;
    border: solid 1px #1F8DD6;
    position: absolute;
    left: 10px;
}
</style>

<div id="quizWrap">
    <div id="Q1" class="quizblock">
         <h4 class="questiontext">1.    What is the gender of the insured party?
(For survivorship policies, select "female")
</h4>

        <ul class="answers" id="q1answers">
            <li class="qwarn hideqs" id="warnq1">Please select one answer.</li>
            <li>
                <input value="0" id="q1_0" name="question1" type="radio">
                <label for="q1_0" id="q1_0label">Female</label>
            </li>
            <li>
                <input value="1" id="q1_1" name="question1" type="radio">
                <label for="q1_1" id="q1_1label">Male</label>
            </li>
        </ul>
        <input type="button" class="btnquiz" id="q1btn" value="Next Question" />
    </div>
    <div id="Q2" class="quizblock hideqs">
         <h4 class="questiontext">2.    What is the age of the insured party?
(For survivorship policies, select the age of the youngest insured party)
</h4>

        <ul class="answers" id="q2answers">
            <li class="qwarn hideqs" id="warnq2">Please select one answer.</li>
            <li>
                <input value="0" id="q2_1" name="question2" type="radio">
                <label for="q2_1" id="q2_1label">64 or less</label>
            </li>
            <li>
                <input value="1" id="q2_2" name="question2" type="radio">
                <label for="q2_2" id="q2_2label">65-77 years old </label>
            </li>
            <li>
                <input value="2" id="q2_3" name="question2" type="radio">
                <label for="q2_3" id="q2_3label">78-81 years old </label>
            </li>
            <li>
                <input value="3" id="q2_4" name="question2" type="radio">
                <label for="q2_4" id="q2_4label">82-86 years old </label>
            </li>
            <li>
                <input value="4" id="q2_0" name="question2" type="radio">
                <label for="q2_0" id="q2_0label">87 years or older </label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q2btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q2btn" value="Next Question" />
    </div>
    <div id="Q3" class="quizblock hideqs">
         <h4 class="questiontext">3.    Describe current health condition of insured party:
(For survivorship policies, select condition of youngest insured party)
</h4>

        <ul class="answers" id="q3answers">
            <li class="qwarn hideqs" id="warnq3">Please select one answer.</li>
            <li>
                <input value="1" id="q3_1" name="question3" type="radio">
                <label for="q3_1" id="q3_1label">In good health </label>
            </li>
            <li>
                <input value="2" id="q3_2" name="question3" type="radio">
                <label for="q3_2" id="q3_2label">Minor health problems </label>
            </li>
            <li>
                <input value="3" id="q3_3" name="question3" type="radio">
                <label for="q3_3" id="q3_3label">Significant health problems </label>
            </li>
            <li>
                <input value="4" id="q3_4" name="question3" type="radio">
                <label for="q3_4" id="q3_4label">Has a terminal or catastrophic illness </label>
            </li>
            <li>
                <input value="0" id="q3_0" name="question3" type="radio">
                <label for="q3_0" id="q3_0label">Unsure</label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q3btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q3btn" value="Next Question" />
    </div>
    <div id="Q4" class="quizblock hideqs">
         <h4 class="questiontext">4.    What type of life insurance policy do you own?</h4>

        <ul class="answers" id="q4answers">
            <li class="qwarn hideqs" id="warnq4">Please select one answer.</li>
            <li>
                <input value="1" id="q4_1" name="question4" type="radio">
                <label for="q4_1" id="q4_1label">Whole Life</label>
            </li>
            <li>
                <input value="2" id="q4_2" name="question4" type="radio">
                <label for="q4_2" id="q4_2label">Joint Survivorship </label>
            </li>
            <li>
                <input value="3" id="q4_3" name="question4" type="radio">
                <label for="q4_3" id="q4_3label">Term </label>
            </li>
            <li>
                <input value="4" id="q4_4" name="question4" type="radio">
                <label for="q4_4" id="q4_4label">Universal Life or Joint Survivorship with one party deceased </label>
            </li>
            <li>
                <input value="0" id="q4_0" name="question4" type="radio">
                <label for="q4_0" id="q4_0label">Unsure</label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q4btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q4btn" value="Next Question" />
    </div>
    <div id="Q5" class="quizblock hideqs">
         <h4 class="questiontext">5.    What is the current cash surrender value of the policy?</h4>

        <ul class="answers" id="q5answers">
            <li class="qwarn hideqs" id="warnq5">Please select one answer.</li>
            <li>
                <input value="4" id="q5_1" name="question5" type="radio">
                <label for="q5_1" id="q5_1label">Less than 10% of the death benefit </label>
            </li>
            <li>
                <input value="3" id="q5_2" name="question5" type="radio">
                <label for="q5_2" id="q5_2label">10-19% of the death benefit </label>
            </li>
            <li>
                <input value="2" id="q5_3" name="question5" type="radio">
                <label for="q5_3" id="q5_3label">20-29% of the death benefit </label>
            </li>
            <li>
                <input value="1" id="q5_4" name="question5" type="radio">
                <label for="q5_4" id="q5_4label">30% or more of the death benefit </label>
            </li>
            <li>
                <input value="0" id="q5_0" name="question5" type="radio">
                <label for="q5_0" id="q5_0label">Unsure</label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q5btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q5btn" value="Next Question" />
    </div>
    <div id="Q6" class="quizblock hideqs">
         <h4 class="questiontext">6.    What are the outstanding loans balance against the policy?</h4>

        <ul class="answers" id="q6answers">
            <li class="qwarn hideqs" id="warnq6">Please select one answer.</li>
            <li>
                <input value="4" id="q6_1" name="question6" type="radio">
                <label for="q6_1" id="q6_1label">Less than 10% of the death benefit </label>
            </li>
            <li>
                <input value="3" id="q6_2" name="question6" type="radio">
                <label for="q6_2" id="q6_2label">10-19% of death benefit </label>
            </li>
            <li>
                <input value="2" id="q6_3" name="question6" type="radio">
                <label for="q6_3" id="q6_3label">20-29% of death benefit </label>
            </li>
            <li>
                <input value="1" id="q6_4" name="question6" type="radio">
                <label for="q6_4" id="q6_4label">30% or more of the death benefit </label>
            </li>
            <li>
                <input value="0" id="q6_0" name="question6" type="radio">
                <label for="q6_0" id="q6_0label">Unsure</label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q6btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q6btn" value="Next Question" />
    </div>
    <div id="Q7" class="quizblock hideqs">
         <h4 class="questiontext">7.    What is the total annual premium payment as a percentage of the death benefit?</h4>

        <ul class="answers" id="q7answers">
            <li class="qwarn hideqs" id="warnq7">Please select one answer.</li>
            <li>
                <input value="4" id="q7_1" name="question7" type="radio">
                <label for="q7_1" id="q7_1label">Less than 2% of death benefit</label>
            </li>
            <li>
                <input value="3" id="q7_2" name="question7" type="radio">
                <label for="q7_2" id="q7_2label">2-3% of the death benefit</label>
            </li>
            <li>
                <input value="2" id="q7_3" name="question7" type="radio">
                <label for="q7_3" id="q7_3label">3-4% of the death benefit</label>
            </li>
            <li>
                <input value="1" id="q7_4" name="question7" type="radio">
                <label for="q7_4" id="q7_4label">4% or more of the death benefit</label>
            </li>
            <li>
                <input value="0" id="q7_0" name="question7" type="radio">
                <label for="q7_0" id="q7_0label">Unsure </label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q7btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q7btn" value="Next Question" />
    </div>
    <div id="Q8" class="quizblock hideqs">
         <h4 class="questiontext">8.    What is the policy death benefit amount?</h4>

        <ul class="answers" id="q8answers">
            <li class="qwarn hideqs" id="warnq8">Please select one answer.</li>
            <li>
                <input value="1" id="q68_1" name="question8" type="radio">
                <label for="q8_1" id="q8_1label">Up to $250,000 </label>
            </li>
            <li>
                <input value="2" id="q8_2" name="question8" type="radio">
                <label for="q8_2" id="q8_2label">$250,001-$5,000,000 </label>
            </li>
            <li>
                <input value="3" id="q8_3" name="question8" type="radio">
                <label for="q8_3" id="q8_3label">$5,000,001 or greater </label>
            </li>
            <li>
                <input value="0" id="q8_0" name="question8" type="radio">
                <label for="q8_0" id="q8_0label">Unsure </label>
            </li>
        </ul>
        <input type="button" class="btnquizprev" id="q8btnback" value="< Previous Question" />
        <input type="button" class="btnquiz" id="q8btn" value="Finish" />
    </div>
    <div id="resultpart1" class="quizblock hideqs">

        <h5>At this time, you do not appear to be a good candidate for a life settlement. However if you like to review your needs and explore what other options may be available to help fund your future, please contact us today. (CTA link and listing)</h5>

    </div>
    <div id="result" class="quizblock hideqs">

        <h5>Enter the following details and click submit to continue: </h5>

        <p><label for="qName" >*Name: </label><br/><input type="text" name="Name" id="qName" placeholder="Name"/></p>
        <p><label for="qEmail">*Email: </label><br/><input type="text" name="Email" id="qEmail" placeholder="Email"/></p>
        <p><label for="qTel">*Phone No: </label><br/><input type="text" name="Tel" id="qTel" placeholder="Phone No."/></p>
        <p><label for="qTel">Mailing Address: </label><br/><textarea name="MailAdd" id="qAdd" placeholder="Mailing Address" rows="4" cols="50"></textarea></p>
        <p><label for="qTel">*Name Of Insured: </label><br/>
        <select name="insuredoption" id="insuredoption">
  <option value="same">Same</option>
  <option value="new">New</option>
</select><p/>
        <p ><input type="text" name="IName" id="IName" placeholder="Insured Name" class="hideqs"/><p/>
  
        <textarea name="Message" rows="20" cols="20" id="qMessage" placeholder="" class="hideqs"></textarea>

        <input type="button" name="submitall" id="submitall" value="Submit" class="btnquiz" />
 

        <span id="totalscore" class="hideqs"></span>

        <span id="part1totalall" class="hideqs"></span>

        <span id="part2total" class="hideqs"></span>

    </div>
    <div id="resultsent" class="quizblock hideqs">
    <h1>Thank you. Your result has been sent! </h1>
    </div>
</div>

<script language="javascript" type="text/javascript">
    function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
 
loadjscssfile("http://fonts.googleapis.com/css?family=Open+Sans:400,300", "css")
loadjscssfile("http://necolas.github.com/normalize.css/3.0.2/normalize.css", "css") 
loadjscssfile("http://yui.yahooapis.com/pure/0.6.0/pure-min.css", "css") 
</script>

<script language="javascript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="quiz.js"></script>